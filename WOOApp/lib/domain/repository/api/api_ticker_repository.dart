import 'package:woo_test/data/api/responses/api_ticker_item.dart';

abstract class ApiTickerRepository {
  Future<List<ApiTickerItem>> tickers();
}
