import 'package:woo_test/data/general/exception_handler_impl.dart';

abstract class ExceptionHandler {
  Future<void> processException(Object error, StackTrace? stack);

  Future<void> addListener(String tag, ExceptionHandlerListener listener);

  Future<void> removeListener(String tag);
}
