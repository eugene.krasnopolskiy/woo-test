import 'package:woo_test/domain/entity/ticker_type.dart';

class TickerItem {
  final String base;
  final String quote;
  final TickerType type;
  final double lastPrice;
  final double volume;

  TickerItem({
    required this.base,
    required this.quote,
    required this.type,
    required this.lastPrice,
    required this.volume,
  });
}
