import 'package:injectable/injectable.dart';
import 'package:woo_test/domain/entity/ticker_item.dart';
import 'package:woo_test/domain/mapper/ticker_item_mapper.dart';
import 'package:woo_test/domain/repository/api/api_ticker_repository.dart';
import 'package:woo_test/general/log_helper.dart';

@LazySingleton()
class GetTickersUseCase with LogHelper {
  static const tag = 'GetTickersUseCase';

  final ApiTickerRepository _apiTickerRepository;
  final TickerItemMapper _tickerItemMapper;

  GetTickersUseCase(
    this._apiTickerRepository,
    this._tickerItemMapper,
  );

  Future<List<TickerItem>> executeAsync() async {
    tLog.d(tag, 'GetFeedUseCase executeAsync');
    final List<TickerItem> result = [];
    final response = await _apiTickerRepository.tickers();

    if (response.isNotEmpty) {
      result.addAll(_tickerItemMapper.mapList(response));
    }

    // emulate a little delay
    await Future.delayed(const Duration(seconds: 1));

    return Future.value(result);
  }
}
