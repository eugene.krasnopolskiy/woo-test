import 'package:injectable/injectable.dart';
import 'package:woo_test/data/api/responses/api_ticker_item.dart';
import 'package:woo_test/domain/entity/ticker_item.dart';
import 'package:woo_test/domain/entity/ticker_type.dart';
import 'package:woo_test/domain/mapper/base/mapper.dart';

@LazySingleton()
class TickerItemMapper extends Mapper<ApiTickerItem, TickerItem> {
  @override
  ApiTickerItem reverseMap(TickerItem value) {
    throw Exception('Not supported');
  }

  @override
  TickerItem map(ApiTickerItem value) {
    TickerType type;

    switch (value.type) {
      case 'FUTURES':
        type = TickerType.futures;
        break;
      case 'SPOT':
      default:
        type = TickerType.spot;
        break;
    }

    return TickerItem(
      base: value.base,
      quote: value.quote,
      type: type,
      lastPrice: value.lastPrice,
      volume: value.volume,
    );
  }
}
