import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';
import 'package:woo_test/domain/repository/general/exception_handler.dart';
import 'package:woo_test/presentation/injection/configure_dependencies.dart';
import 'package:woo_test/presentation/ui/woo_app.dart';

import 'general/constants.dart';

void main() {
  runZonedGuarded(() {
    WidgetsFlutterBinding.ensureInitialized();
    Future<void> configure;

    if (kReleaseMode) {
      configure = configureDependencies(Environment.prod);
    } else {
      configure = configureDependencies(Environment.dev);
    }

    configure.whenComplete(() {
      // sure that all dependencies injected
      GetIt.instance.allReady().whenComplete(() {
        SystemChrome.setPreferredOrientations(Constants.orientations).whenComplete(() {
          runApp(const WooApp());
        });
      });
    });
  }, (Object error, StackTrace stack) {
    final ExceptionHandler handler = getIt<ExceptionHandler>();
    handler.processException(error, stack);
  });
}
