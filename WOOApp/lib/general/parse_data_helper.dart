mixin ParseDataHelper {
  static double parseDoubleWithNaN(Map<String, dynamic> json, String fieldName) {
    if (json.containsKey(fieldName)) {
      if (json[fieldName] is String && json[fieldName] == 'NaN') {
        return 0.0;
      } else {
        return double.tryParse(json[fieldName].toString().replaceAll(',', '')) ?? 0;
      }
    } else {
      return 0.0;
    }
  }

  static int parseIntWithNaN(Map<String, dynamic> json, String fieldName) {
    if (json.containsKey(fieldName)) {
      if (json[fieldName] is String && json[fieldName] == 'NaN') {
        return 0;
      } else {
        return int.tryParse(json[fieldName].toString()) ?? 0;
      }
    } else {
      return 0;
    }
  }

  static String parseString(Map<String, dynamic> json, String fieldName) {
    if (json.containsKey(fieldName)) {
      if (json[fieldName] is String) {
        return json[fieldName] as String;
      } else {
        return '';
      }
    } else {
      return '';
    }
  }

  static bool parseBool({required Map<String, dynamic> json, required String fieldName, bool defaultValue = false}) {
    if (json.containsKey(fieldName)) {
      if (json[fieldName] is bool) {
        return json[fieldName] as bool;
      } else {
        return defaultValue;
      }
    } else {
      return defaultValue;
    }
  }
}
