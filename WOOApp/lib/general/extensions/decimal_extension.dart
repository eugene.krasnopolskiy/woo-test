import 'package:decimal/decimal.dart';

extension DecimalExtensionDouble on double {
  Decimal toDecimal() {
    return Decimal.tryParse(toString()) ?? Decimal.parse('0');
  }
}
