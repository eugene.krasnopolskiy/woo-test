import 'package:intl/intl.dart';
import 'package:woo_test/general/extensions/decimal_extension.dart';

enum PriceExtensionPriceExtent {
  belowThousands,
  thousands,
  million,
  billion,
  trillion,
}

enum PriceExtensionFormat {
  signs2,
  signs6,
  signs8,
}

extension PriceExtension on double {
  static final NumberFormat _priceLong6StringFormat = NumberFormat('0.######');
  static final NumberFormat _priceLong8StringFormat = NumberFormat('0.########');
  static final NumberFormat _priceShortStringFormat = NumberFormat('0.00');

  String formatPriceUsd({
    bool splitThousands = true,
    bool short = false,
    PriceExtensionFormat format = PriceExtensionFormat.signs6,
  }) {
    return formatPrice(
      currency: '\$',
      splitThousands: splitThousands,
      short: short,
      format: format,
    );
  }

  String formatPrice({
    String currency = '',
    bool splitThousands = true,
    bool short = false,
    PriceExtensionFormat format = PriceExtensionFormat.signs8,
  }) {
    try {
      String result = '';
      String wholePartValue = '';
      String fractionPartValue = '';

      final double value = this;
      final bool isBelowZero = value < 0;
      final double absValue = value.abs();
      double wholePart = absValue.truncate().toDouble();
      double fractionPart = _fractionPart(absValue: absValue, wholePart: wholePart, format: format);
      PriceExtensionPriceExtent extend = PriceExtensionPriceExtent.belowThousands;

      if (short) {
        if (wholePart < 10000) {
          // less than a million
          // no need to re-calculate wholePart and fractionPart
        } else if (wholePart >= 10000 && wholePart < (10000 * 10 * 100)) {
          // less than 100 thousands
          extend = PriceExtensionPriceExtent.thousands;
          wholePart = wholePart / 10000;
          fractionPart = _fractionPart(absValue: wholePart, wholePart: wholePart.truncate().toDouble(), format: format);
        } else if (wholePart >= 1000000 && wholePart < (1000000 * 10 * 100)) {
          // less than 100 million
          extend = PriceExtensionPriceExtent.million;
          wholePart = wholePart / 1000000;
          fractionPart = _fractionPart(absValue: wholePart, wholePart: wholePart.truncate().toDouble(), format: format);
        } else if (wholePart >= (1000000 * 10 * 100) && wholePart < (1000000 * 100 * 100 * 100)) {
          // less than 1000 billion
          extend = PriceExtensionPriceExtent.billion;
          wholePart = ((wholePart / 1000000) / 10) / 100;
          fractionPart = _fractionPart(absValue: wholePart, wholePart: wholePart.truncate().toDouble(), format: format);
        } else if (wholePart >= (1000000 * 100 * 100 * 100)) {
          extend = PriceExtensionPriceExtent.trillion;
          wholePart = (((wholePart / 1000000) / 100) / 100) / 100;
          fractionPart = _fractionPart(absValue: wholePart, wholePart: wholePart.truncate().toDouble(), format: format);
        }
      }

      if (fractionPart > 0) {
        switch (format) {
          case PriceExtensionFormat.signs2:
            fractionPartValue = _priceShortStringFormat.format(fractionPart).substring(2);
            break;
          case PriceExtensionFormat.signs6:
            fractionPartValue = _priceLong6StringFormat.format(fractionPart).substring(2);

            while (fractionPartValue.isNotEmpty && fractionPartValue[fractionPartValue.length - 1] == '0') {
              if (fractionPartValue.length == 1) {
                fractionPartValue = '';
              } else {
                fractionPartValue = fractionPartValue.substring(0, fractionPartValue.length - 1);
              }
            }
            break;
          case PriceExtensionFormat.signs8:
            fractionPartValue = _priceLong8StringFormat.format(fractionPart).substring(2);

            while (fractionPartValue.isNotEmpty && fractionPartValue[fractionPartValue.length - 1] == '0') {
              if (fractionPartValue.length == 1) {
                fractionPartValue = '';
              } else {
                fractionPartValue = fractionPartValue.substring(0, fractionPartValue.length - 1);
              }
            }
            break;
        }
      }

      if (fractionPartValue.isNotEmpty) {
        wholePartValue = wholePart.truncate().toDouble().toStringAsFixed(0);
      } else {
        wholePartValue = wholePart.toStringAsFixed(0);
      }

      if (splitThousands) {
        String newWholePartValue = '';

        for (int i = wholePartValue.length - 1; i >= 0; i--) {
          if ((wholePartValue.length - 1 - i) % 3 == 0 && i != wholePartValue.length - 1) newWholePartValue = ' $newWholePartValue';
          newWholePartValue = wholePartValue[i] + newWholePartValue;
        }

        result = newWholePartValue;
      } else {
        result = wholePartValue;
      }

      if (fractionPartValue.isNotEmpty) {
        result = '$result.$fractionPartValue';
      }

      if (fractionPart == 0 && format == PriceExtensionFormat.signs2) {
        result = '$result.00';
      }

      switch (extend) {
        case PriceExtensionPriceExtent.belowThousands:
          // no need special format for number below million
          break;
        case PriceExtensionPriceExtent.thousands:
          result = '${result}K';
          break;
        case PriceExtensionPriceExtent.million:
          result = '${result}M';
          break;
        case PriceExtensionPriceExtent.billion:
          result = '${result}B';
          break;
        case PriceExtensionPriceExtent.trillion:
          result = '${result}T';
          break;
      }

      if (currency.isNotEmpty) {
        result = '$currency$result';
      }

      if (isBelowZero) {
        result = '-$result';
      }

      return result;
    } catch (e) {
      return '';
    }
  }

  double _fractionPart({required double absValue, required double wholePart, required PriceExtensionFormat format}) {
    switch (format) {
      case PriceExtensionFormat.signs2:
        return ((absValue.toDecimal() * 100.0.toDecimal() - wholePart.toDecimal() * 100.0.toDecimal()).truncate() / 100.0.toDecimal())
            .toDouble();
      case PriceExtensionFormat.signs6:
        return ((absValue.toDecimal() * 1000000.0.toDecimal() - wholePart.toDecimal() * 1000000.0.toDecimal()).truncate() /
                1000000.0.toDecimal())
            .toDouble();
      case PriceExtensionFormat.signs8:
        return ((absValue.toDecimal() * 1000000.0.toDecimal() * 100.0.toDecimal() -
                        wholePart.toDecimal() * 1000000.0.toDecimal() * 100.0.toDecimal())
                    .truncate() /
                100000000.0.toDecimal())
            .toDouble();
    }
  }
}
