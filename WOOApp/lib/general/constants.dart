import 'package:flutter/services.dart';

class Constants {
  // list of default orientation
  static const List<DeviceOrientation> orientations = [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown];

  static const symbolBtc = 'BTC';
  static const symbolEth = 'ETH';
  static const symbolWoo = 'WOO';
}
