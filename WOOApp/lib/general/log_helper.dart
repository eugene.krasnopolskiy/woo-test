import 'package:woo_test/domain/repository/log/log.dart';
import 'package:woo_test/presentation/injection/configure_dependencies.dart';

mixin LogHelper {
  final Log tLog = getIt<Log>();
}