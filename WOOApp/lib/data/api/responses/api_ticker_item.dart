import 'package:woo_test/general/parse_data_helper.dart';

class ApiTickerItem {
  final String base;
  final String quote;
  final String type;
  final double lastPrice;
  final double volume;

  ApiTickerItem({
    required this.base,
    required this.quote,
    required this.type,
    required this.lastPrice,
    required this.volume,
  });

  factory ApiTickerItem.fromJson(Map<String, dynamic> json) {
    return ApiTickerItem(
      base: ParseDataHelper.parseString(json, 'base'),
      quote: ParseDataHelper.parseString(json, 'quote'),
      type: ParseDataHelper.parseString(json, 'type'),
      lastPrice: ParseDataHelper.parseDoubleWithNaN(json, 'lastPrice'),
      volume: ParseDataHelper.parseDoubleWithNaN(json, 'volume'),
    );
  }
}
