import 'package:flutter/material.dart';
import 'package:woo_test/design/app_colors.dart';
import 'package:woo_test/presentation/ui/screens/tickers_screen.dart';

class WooApp extends StatelessWidget {
  const WooApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'WOO',
      theme: ThemeData(
        primarySwatch: AppColors.createMaterialColor(AppColors.backgroundMainColor),
        brightness: Brightness.light,
      ),
      darkTheme: ThemeData(
        primarySwatch: AppColors.createMaterialColor(AppColors.backgroundMainColor),
        brightness: Brightness.dark,
      ),
      themeMode: ThemeMode.dark,
      debugShowCheckedModeBanner: false,
      home: const TickersScreen(),
    );
  }
}
