import 'package:flutter/material.dart';
import 'package:woo_test/design/app_colors.dart';
import 'package:woo_test/design/design_text_style_tokens.dart';
import 'package:woo_test/domain/entity/sort_direction.dart';
import 'package:woo_test/domain/entity/sort_type.dart';
import 'package:woo_test/domain/entity/ticker_item.dart';
import 'package:woo_test/domain/entity/ticker_type.dart';
import 'package:woo_test/domain/usecases/feed/get_tickers_use_case.dart';
import 'package:woo_test/general/constants.dart';
import 'package:woo_test/presentation/injection/configure_dependencies.dart';
import 'package:woo_test/presentation/ui/widgets/ticker_item_widget.dart';
import 'package:woo_test/presentation/ui/widgets/ticker_sort_panel_widget.dart';

class TickersScreen extends StatefulWidget {
  static const tag = 'FeedScreen';

  const TickersScreen({Key? key}) : super(key: key);

  @override
  _TickersScreenState createState() {
    return _TickersScreenState();
  }
}

class _TickersScreenState extends State<TickersScreen> with SingleTickerProviderStateMixin {
  final _designTextStyleTokens = getIt<DesignTextStyleTokens>();
  final _signInUseCase = getIt<GetTickersUseCase>();
  final _scrollController = ScrollController();
  late TabController _tabController;

  final List<TickerItem> _items = [];
  SortType _sortTypeAll = SortType.symbol;
  SortDirection _sortDirectionAll = SortDirection.none;

  SortType _sortTypeStop = SortType.symbol;
  SortDirection _sortDirectionSpot = SortDirection.none;

  SortType _sortTypeFutures = SortType.symbol;
  SortDirection _sortDirectionFutures = SortDirection.none;

  bool _loading = false;

  final _searchController = TextEditingController();

  String _searchText = '';

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 3, vsync: this);
    _tabController.addListener(() {
      _searchController.clear();
      _searchText = '';
    });
    _loadData();
  }

  @override
  void dispose() {
    _searchController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.backgroundMainColor,
      appBar: AppBar(
        title: const Text('Tickers'),
      ),
      resizeToAvoidBottomInset: false,
      body: _buildContent(),
    );
  }

  Widget _buildList() {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          TabBar(
            labelStyle: _designTextStyleTokens.tickerText(),
            unselectedLabelStyle: _designTextStyleTokens.tickerInfo(),
            tabs: const [Tab(text: 'ALL'), Tab(text: 'SPOT'), Tab(text: 'FUTURES')],
            controller: _tabController,
            indicatorSize: TabBarIndicatorSize.tab,
          ),
          Container(
            margin: const EdgeInsets.all(16),
            child: TextFormField(
              controller: _searchController,
              cursorColor: AppColors.textMainColor,
              decoration: InputDecoration(
                border: const OutlineInputBorder(
                  borderSide: BorderSide(color: AppColors.textMainColor),
                ),
                enabledBorder: const OutlineInputBorder(
                  borderSide: BorderSide(color: AppColors.textMainColor),
                ),
                focusedBorder: const OutlineInputBorder(
                  borderSide: BorderSide(color: AppColors.textMainColor),
                ),
                labelStyle: _designTextStyleTokens.tickerInfo(),
                labelText: 'Enter search text',
                focusColor: AppColors.textMainColor,
              ),
              onChanged: (value) {
                setState(() {
                  _searchText = value;
                });
              },
            ),
          ),
          Expanded(
            child: TabBarView(
              children: [
                _buildAll(),
                _buildSpot(),
                _buildFeatures(),
              ],
              controller: _tabController,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildNoResults() {
    return Center(
      child: Text(
        'No results',
        style: _designTextStyleTokens.tickerText(),
      ),
    );
  }

  Widget _buildAll() {
    final List<TickerItem> items = _prepareItemsAll(_sortTypeAll, _sortDirectionAll,
        _searchText.isNotEmpty ? _items.where((element) => element.base.toLowerCase().contains(_searchText.toLowerCase())) : _items);

    if (items.isEmpty) {
      return _buildNoResults();
    } else {
      return Column(
        children: [
          TickerSortPanelWidget(
            onChanged: (SortType sortType, SortDirection sortDirection) {
              setState(() {
                _sortTypeAll = sortType;
                _sortDirectionAll = sortDirection;
              });
            },
            designTextStyleTokens: _designTextStyleTokens,
            sortTypeDefault: _sortTypeAll,
            sortDirectionDefault: _sortDirectionAll,
          ),
          Expanded(
            child: ListView.separated(
              controller: _scrollController,
              itemCount: items.length,
              shrinkWrap: true,
              itemBuilder: (BuildContext context, int index) {
                return TickerItemWidget(
                  designTextStyleTokens: _designTextStyleTokens,
                  item: items[index],
                );
              },
              separatorBuilder: (BuildContext context, int index) => Container(
                height: 1,
                color: AppColors.textSecondaryColor,
              ),
            ),
          )
        ],
      );
    }
  }

  Widget _buildSpot() {
    final List<TickerItem> items = _prepareItemsSpot(
        _sortTypeStop,
        _sortDirectionSpot,
        _searchText.isNotEmpty
            ? _items.where((element) => element.type == TickerType.spot && element.base.toLowerCase().contains(_searchText.toLowerCase()))
            : _items.where((element) => element.type == TickerType.spot));

    if (items.isEmpty) {
      return _buildNoResults();
    } else {
      return Column(
        children: [
          TickerSortPanelWidget(
            onChanged: (SortType sortType, SortDirection sortDirection) {
              setState(() {
                _sortTypeStop = sortType;
                _sortDirectionSpot = sortDirection;
              });
            },
            designTextStyleTokens: _designTextStyleTokens,
            sortTypeDefault: _sortTypeStop,
            sortDirectionDefault: _sortDirectionSpot,
          ),
          Expanded(
            child: ListView.separated(
              controller: _scrollController,
              itemCount: items.length,
              shrinkWrap: true,
              itemBuilder: (BuildContext context, int index) {
                return TickerItemWidget(
                  designTextStyleTokens: _designTextStyleTokens,
                  item: items[index],
                );
              },
              separatorBuilder: (BuildContext context, int index) => Container(
                height: 1,
                color: AppColors.textSecondaryColor,
              ),
            ),
          )
        ],
      );
    }
  }

  Widget _buildFeatures() {
    final List<TickerItem> items = _prepareItemsFutures(
        _sortTypeStop,
        _sortDirectionSpot,
        _searchText.isNotEmpty
            ? _items
                .where((element) => element.type == TickerType.futures && element.base.toLowerCase().contains(_searchText.toLowerCase()))
            : _items.where((element) => element.type == TickerType.futures));

    if (items.isEmpty) {
      return _buildNoResults();
    } else {
      return Column(
        children: [
          TickerSortPanelWidget(
            onChanged: (SortType sortType, SortDirection sortDirection) {
              setState(() {
                _sortTypeFutures = sortType;
                _sortDirectionFutures = sortDirection;
              });
            },
            designTextStyleTokens: _designTextStyleTokens,
            sortTypeDefault: _sortTypeFutures,
            sortDirectionDefault: _sortDirectionFutures,
          ),
          Expanded(
            child: ListView.separated(
              controller: _scrollController,
              itemCount: items.length,
              shrinkWrap: true,
              itemBuilder: (BuildContext context, int index) {
                return TickerItemWidget(
                  designTextStyleTokens: _designTextStyleTokens,
                  item: items[index],
                );
              },
              separatorBuilder: (BuildContext context, int index) => Container(
                height: 1,
                color: AppColors.textSecondaryColor,
              ),
            ),
          )
        ],
      );
    }
  }

  Widget _buildContent() {
    if (_loading) {
      return _buildProgress();
    } else {
      return _buildList();
    }
  }

  Widget _buildProgress() {
    return const Center(
      child: SizedBox(
        width: 48,
        height: 48,
        child: CircularProgressIndicator(
          color: AppColors.textMainColor,
          strokeWidth: 2,
        ),
      ),
    );
  }

  Future<void> _loadData() async {
    if (!_loading) {
      setState(() {
        _loading = true;
      });

      await _signInUseCase.executeAsync().then((value) {
        setState(() {
          _items.clear();
          _items.addAll(value);
        });
      }).whenComplete(() {
        setState(() {
          _loading = false;
        });
      });
    }
  }

  List<TickerItem> _prepareItemsAll(SortType type, SortDirection direction, Iterable<TickerItem> items) {
    List<TickerItem> result;

    switch (direction) {
      case SortDirection.none:
        result = [];
        final btcList = items.where((element) => element.base == Constants.symbolBtc).toList();
        btcList.sort((a, b) => _compareQuote(a, b));

        final ethList = items.where((element) => element.base == Constants.symbolEth).toList();
        ethList.sort((a, b) => _compareQuote(a, b));

        final wooList = items.where((element) => element.base == Constants.symbolWoo).toList();
        wooList.sort((a, b) => _compareQuote(a, b));

        final allList = items
            .where((element) =>
                element.base != Constants.symbolBtc && element.base != Constants.symbolEth && element.base != Constants.symbolWoo)
            .toList();
        allList.sort((a, b) {
          int result = a.base.compareTo(b.base);

          if (result == 0) {
            result = _compareQuote(a, b);
          }

          return result;
        });

        result.addAll(btcList);
        result.addAll(ethList);
        result.addAll(wooList);
        result.addAll(allList);
        break;
      case SortDirection.ascending:
        result = _prepareItemsAscending(type, items);
        break;
      case SortDirection.descending:
        result = _prepareItemsDescending(type, items);
        break;
    }

    return result;
  }

  List<TickerItem> _prepareItemsSpot(SortType type, SortDirection direction, Iterable<TickerItem> items) {
    List<TickerItem> result;

    switch (direction) {
      case SortDirection.none:
        result = [];
        final btcList = items.where((element) => element.base == Constants.symbolBtc).toList();
        btcList.sort((a, b) => _compareQuote(a, b));

        final ethList = items.where((element) => element.base == Constants.symbolEth).toList();
        ethList.sort((a, b) => _compareQuote(a, b));

        final wooList = items.where((element) => element.base == Constants.symbolWoo).toList();
        wooList.sort((a, b) => _compareQuote(a, b));

        final allList = items
            .where((element) =>
                element.base != Constants.symbolBtc && element.base != Constants.symbolEth && element.base != Constants.symbolWoo)
            .toList();
        allList.sort((a, b) => b.volume.compareTo(a.volume));

        result.addAll(btcList);
        result.addAll(ethList);
        result.addAll(wooList);
        result.addAll(allList);
        break;
      case SortDirection.ascending:
        result = _prepareItemsAscending(type, items);
        break;
      case SortDirection.descending:
        result = _prepareItemsDescending(type, items);
        break;
    }

    return result;
  }

  List<TickerItem> _prepareItemsFutures(SortType type, SortDirection direction, Iterable<TickerItem> items) {
    List<TickerItem> result;

    switch (direction) {
      case SortDirection.none:
        result = [];
        final btcList = items.where((element) => element.base == Constants.symbolBtc).toList();
        btcList.sort((a, b) => _compareQuote(a, b));

        final ethList = items.where((element) => element.base == Constants.symbolEth).toList();
        ethList.sort((a, b) => _compareQuote(a, b));

        final wooList = items.where((element) => element.base == Constants.symbolWoo).toList();
        wooList.sort((a, b) => _compareQuote(a, b));

        final allList = items
            .where((element) =>
                element.base != Constants.symbolBtc && element.base != Constants.symbolEth && element.base != Constants.symbolWoo)
            .toList();
        allList.sort((a, b) => b.volume.compareTo(a.volume));

        result.addAll(btcList);
        result.addAll(ethList);
        result.addAll(wooList);
        result.addAll(allList);
        break;
      case SortDirection.ascending:
        result = _prepareItemsAscending(type, items);
        break;
      case SortDirection.descending:
        result = _prepareItemsDescending(type, items);
        break;
    }

    return result;
  }

  List<TickerItem> _prepareItemsDescending(SortType type, Iterable<TickerItem> items) {
    List<TickerItem> result;

    switch (type) {
      case SortType.symbol:
        result = items.toList();
        result.sort((a, b) {
          int result = b.base.compareTo(a.base);

          if (result == 0) {
            result = _compareQuote(b, a);

            if (result == 0) {
              result = _compareType(b, a);
            }
          }

          return result;
        });
        break;
      case SortType.lastPrice:
        result = items.toList();
        result.sort((a, b) => b.lastPrice.compareTo(a.lastPrice));
        break;
      case SortType.volume:
        result = items.toList();
        result.sort((a, b) => b.volume.compareTo(a.volume));
        break;
    }

    return result;
  }

  List<TickerItem> _prepareItemsAscending(SortType type, Iterable<TickerItem> items) {
    List<TickerItem> result;

    switch (type) {
      case SortType.symbol:
        result = items.toList();
        result.sort((a, b) {
          int result = a.base.compareTo(b.base);

          if (result == 0) {
            result = _compareQuote(a, b);

            if (result == 0) {
              result = _compareType(a, b);
            }
          }

          return result;
        });
        break;
      case SortType.lastPrice:
        result = items.toList();
        result.sort((a, b) => a.lastPrice.compareTo(b.lastPrice));
        break;
      case SortType.volume:
        result = items.toList();
        result.sort((a, b) => a.volume.compareTo(b.volume));
        break;
    }

    return result;
  }

  int _compareType(TickerItem a, TickerItem b) {
    switch (a.type) {
      case TickerType.spot:
        switch (b.type) {
          case TickerType.spot:
            return 0;
          case TickerType.futures:
            return -1;
        }
      case TickerType.futures:
        switch (b.type) {
          case TickerType.spot:
            return 1;
          case TickerType.futures:
            return 0;
        }
    }
  }

  int _compareQuote(TickerItem a, TickerItem b) {
    switch (a.quote) {
      case 'USDT':
        switch (b.quote) {
          case 'USDT':
            return 0;
          case 'USDC':
            return -1;
          case 'PERP':
            return -1;
        }
        break;
      case 'USDC':
        switch (b.quote) {
          case 'USDT':
            return -1;
          case 'USDC':
            return 0;
          case 'PERP':
            return 1;
        }
        break;
      case 'PERP':
        switch (b.quote) {
          case 'USDT':
            return -1;
          case 'USDC':
            return -1;
          case 'PERP':
            return 0;
        }
        break;
    }

    return 0;
  }
}
