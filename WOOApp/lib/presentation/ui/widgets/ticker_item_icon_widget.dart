import 'package:flutter/material.dart';
import 'package:woo_test/design/app_colors.dart';
import 'package:woo_test/design/design_text_style_tokens.dart';
import 'package:woo_test/presentation/injection/configure_dependencies.dart';

class TickerItemIconWidget extends StatelessWidget {
  final designTextStyleTokens = getIt<DesignTextStyleTokens>();

  final String ticker;
  final double size;

  TickerItemIconWidget({
    Key? key,
    required this.ticker,
    required this.size,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      decoration: BoxDecoration(
        border: const Border(
          top: BorderSide(
            color: AppColors.textSecondaryColor,
            width: 0.5,
          ),
          right: BorderSide(
            color: AppColors.textSecondaryColor,
            width: 0.5,
          ),
          bottom: BorderSide(
            color: AppColors.textSecondaryColor,
            width: 0.5,
          ),
          left: BorderSide(
            color: AppColors.textSecondaryColor,
            width: 0.5,
          ),
        ),
        borderRadius: BorderRadius.circular(size),
      ),
      width: size,
      height: size,
      child: Text(
        ticker.length > 3 ? ticker.substring(0, 3).toUpperCase() : ticker.toUpperCase(),
        style: designTextStyleTokens.tickerIconText(),
      ),
    );
  }
}
