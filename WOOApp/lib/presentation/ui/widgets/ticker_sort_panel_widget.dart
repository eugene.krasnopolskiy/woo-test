import 'package:flutter/material.dart';
import 'package:woo_test/design/app_colors.dart';
import 'package:woo_test/design/design_text_style_tokens.dart';
import 'package:woo_test/domain/entity/sort_direction.dart';
import 'package:woo_test/domain/entity/sort_type.dart';

class TickerSortPanelWidget extends StatefulWidget {
  final DesignTextStyleTokens designTextStyleTokens;
  final Function(SortType sortType, SortDirection sortDirection) onChanged;

  final SortType sortTypeDefault;
  final SortDirection sortDirectionDefault;

  const TickerSortPanelWidget({
    required this.sortTypeDefault,
    required this.sortDirectionDefault,
    required this.designTextStyleTokens,
    required this.onChanged,
    Key? key,
  }) : super(key: key);

  @override
  _TickerSortPanelWidgetState createState() => _TickerSortPanelWidgetState();
}

class _TickerSortPanelWidgetState extends State<TickerSortPanelWidget> {
  SortType _sortType = SortType.symbol;
  SortDirection _sortDirection = SortDirection.none;

  @override
  void initState() {
    super.initState();
    _sortType = widget.sortTypeDefault;
    _sortDirection = widget.sortDirectionDefault;
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Expanded(
          child: GestureDetector(
            onTap: () => _updateSort(SortType.symbol),
            child: Container(
              margin: const EdgeInsets.all(16),
              alignment: Alignment.center,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Symbol',
                    style: _sortDirection != SortDirection.none && _sortType == SortType.symbol
                        ? widget.designTextStyleTokens.tickerText()
                        : widget.designTextStyleTokens.tickerInfo(),
                  ),
                  Container(
                    width: 8,
                    height: 8,
                    margin: const EdgeInsets.only(left: 8),
                    decoration: BoxDecoration(
                      color: _sortType != SortType.symbol
                          ? AppColors.sortDirectionNone
                          : _sortDirection == SortDirection.ascending
                              ? AppColors.sortDirectionAscending
                              : _sortDirection == SortDirection.descending
                                  ? AppColors.sortDirectionDescending
                                  : AppColors.sortDirectionNone,
                      borderRadius: BorderRadius.circular(8),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        Expanded(
          child: GestureDetector(
            onTap: () => _updateSort(SortType.lastPrice),
            child: Container(
              margin: const EdgeInsets.all(16),
              alignment: Alignment.center,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Last Price',
                    style: _sortDirection != SortDirection.none && _sortType == SortType.lastPrice
                        ? widget.designTextStyleTokens.tickerText()
                        : widget.designTextStyleTokens.tickerInfo(),
                  ),
                  Container(
                    width: 8,
                    height: 8,
                    margin: const EdgeInsets.only(left: 8),
                    decoration: BoxDecoration(
                      color: _sortType != SortType.lastPrice
                          ? AppColors.sortDirectionNone
                          : _sortDirection == SortDirection.ascending
                              ? AppColors.sortDirectionAscending
                              : _sortDirection == SortDirection.descending
                                  ? AppColors.sortDirectionDescending
                                  : AppColors.sortDirectionNone,
                      borderRadius: BorderRadius.circular(8),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        Expanded(
          child: GestureDetector(
            onTap: () => _updateSort(SortType.volume),
            child: Container(
              margin: const EdgeInsets.all(16),
              alignment: Alignment.center,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Volume',
                    style: _sortDirection != SortDirection.none && _sortType == SortType.volume
                        ? widget.designTextStyleTokens.tickerText()
                        : widget.designTextStyleTokens.tickerInfo(),
                  ),
                  Container(
                    width: 8,
                    height: 8,
                    margin: const EdgeInsets.only(left: 8),
                    decoration: BoxDecoration(
                      color: _sortType != SortType.volume
                          ? AppColors.sortDirectionNone
                          : _sortDirection == SortDirection.ascending
                              ? AppColors.sortDirectionAscending
                              : _sortDirection == SortDirection.descending
                                  ? AppColors.sortDirectionDescending
                                  : AppColors.sortDirectionNone,
                      borderRadius: BorderRadius.circular(8),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  void _updateSort(SortType type) {
    setState(() {
      _sortDirection = (_sortType == type || _sortDirection == SortDirection.none) ? _getNextSortDirection(_sortDirection) : _sortDirection;
      _sortType = type;
      widget.onChanged(_sortType, _sortDirection);
    });
  }

  SortDirection _getNextSortDirection(SortDirection direction) {
    switch (direction) {
      case SortDirection.none:
        return SortDirection.ascending;
      case SortDirection.ascending:
        return SortDirection.descending;
      case SortDirection.descending:
        return SortDirection.none;
    }
  }
}
