import 'package:flutter/material.dart';
import 'package:woo_test/design/app_colors.dart';
import 'package:woo_test/design/design_text_style_tokens.dart';
import 'package:woo_test/domain/entity/ticker_item.dart';
import 'package:woo_test/domain/entity/ticker_type.dart';
import 'package:woo_test/general/extensions/price_extension.dart';

class TickerItemWidget extends StatefulWidget {
  final TickerItem item;
  final DesignTextStyleTokens designTextStyleTokens;

  const TickerItemWidget({
    required this.item,
    required this.designTextStyleTokens,
    Key? key,
  }) : super(key: key);

  @override
  _TickerItemWidgetState createState() => _TickerItemWidgetState();
}

class _TickerItemWidgetState extends State<TickerItemWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(color: AppColors.backgroundMainColor),
      padding: const EdgeInsets.only(top: 16, bottom: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Row(
            mainAxisSize: MainAxisSize.max,
            children: [
              Expanded(
                child: Container(
                  alignment: Alignment.centerLeft,
                  margin: const EdgeInsets.only(left: 16, right: 8),
                  child: Text(
                    _prepareTickerName(),
                    maxLines: 1,
                    textAlign: TextAlign.start,
                    style: widget.designTextStyleTokens.tickerInfo(),
                    overflow: TextOverflow.ellipsis,
                    softWrap: false,
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  alignment: Alignment.centerRight,
                  margin: const EdgeInsets.only(right: 16),
                  child: Text(
                    _preparePrice(),
                    maxLines: 1,
                    textAlign: TextAlign.start,
                    style: widget.designTextStyleTokens.tickerText(),
                    overflow: TextOverflow.ellipsis,
                    softWrap: false,
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  alignment: Alignment.centerRight,
                  margin: const EdgeInsets.only(right: 16),
                  child: Text(
                    _prepareVolume(),
                    maxLines: 1,
                    textAlign: TextAlign.start,
                    style: widget.designTextStyleTokens.tickerInfo(),
                    overflow: TextOverflow.ellipsis,
                    softWrap: false,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  String _prepareTickerName() {
    switch (widget.item.type) {
      case TickerType.spot:
        return '${widget.item.base}/${widget.item.quote}';
      case TickerType.futures:
        return '${widget.item.base}-PERP';
    }
  }

  String _preparePrice() {
    if (widget.item.lastPrice < 1) {
      return widget.item.lastPrice.formatPriceUsd(format: PriceExtensionFormat.signs6);
    } else {
      return widget.item.lastPrice.formatPriceUsd(format: PriceExtensionFormat.signs2);
    }
  }

  String _prepareVolume() {
    return widget.item.volume.formatPriceUsd(format: PriceExtensionFormat.signs2, short: true);
  }
}
