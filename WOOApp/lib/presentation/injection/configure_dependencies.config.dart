// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;

import '../../data/api/api_ticker_repository_impl.dart' as _i4;
import '../../data/general/exception_handler_impl.dart' as _i7;
import '../../data/log/log_impl.dart' as _i9;
import '../../design/design_text_style_tokens.dart' as _i5;
import '../../domain/mapper/ticker_item_mapper.dart' as _i10;
import '../../domain/repository/api/api_ticker_repository.dart' as _i3;
import '../../domain/repository/general/exception_handler.dart' as _i6;
import '../../domain/repository/log/log.dart' as _i8;
import '../../domain/usecases/feed/get_tickers_use_case.dart'
    as _i11; // ignore_for_file: unnecessary_lambdas

// ignore_for_file: lines_longer_than_80_chars
/// initializes the registration of provided dependencies inside of [GetIt]
_i1.GetIt $initGetIt(_i1.GetIt get,
    {String? environment, _i2.EnvironmentFilter? environmentFilter}) {
  final gh = _i2.GetItHelper(get, environment, environmentFilter);
  gh.lazySingleton<_i3.ApiTickerRepository>(
      () => _i4.ApiTickerRepositoryImpl());
  gh.singleton<_i5.DesignTextStyleTokens>(_i5.DesignTextStyleTokens());
  gh.singleton<_i6.ExceptionHandler>(_i7.ExceptionHandlerImpl());
  gh.lazySingleton<_i8.Log>(() => _i9.LogImpl());
  gh.lazySingleton<_i10.TickerItemMapper>(() => _i10.TickerItemMapper());
  gh.lazySingleton<_i11.GetTickersUseCase>(() => _i11.GetTickersUseCase(
      get<_i3.ApiTickerRepository>(), get<_i10.TickerItemMapper>()));
  return get;
}
