import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';
import 'package:woo_test/presentation/injection/configure_dependencies.config.dart';

final getIt = GetIt.instance;

@InjectableInit()
Future<void> configureDependencies(String env) async {
  $initGetIt(getIt, environment: env);
  return Future.value();
}
