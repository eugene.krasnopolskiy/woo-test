import 'package:flutter/material.dart';

class AppColors {
  static const Color backgroundMainColor = Color(0xFF1F1F1F);
  static const Color backgroundSecondary = Color(0xFF161616);

  static const Color separatorColor = Color(0xFF3B3B3B);

  static const Color buttonBackgroundColor = Color(0xFF737373);

  static const Color textMainColor = Color(0xFFF6F6F6);
  static const Color textSecondaryColor = Color(0xFF737373);
  static const Color textAccentColor = Color(0xFFB23634);

  static const Color sortDirectionNone = Colors.transparent;
  static const Color sortDirectionAscending = Color(0xFF56B778);
  static const Color sortDirectionDescending = Color(0xFFB23634);

  static MaterialColor createMaterialColor(Color color) {
    List strengths = <double>[.05];
    Map<int, Color> swatch = {};
    final int r = color.red, g = color.green, b = color.blue;

    for (int i = 1; i < 10; i++) {
      strengths.add(0.1 * i);
    }
    for (var strength in strengths) {
      final double ds = 0.5 - strength;
      swatch[(strength * 1000).round()] = Color.fromRGBO(
        r + ((ds < 0 ? r : (255 - r)) * ds).round(),
        g + ((ds < 0 ? g : (255 - g)) * ds).round(),
        b + ((ds < 0 ? b : (255 - b)) * ds).round(),
        1,
      );
    }
    return MaterialColor(color.value, swatch);
  }
}
