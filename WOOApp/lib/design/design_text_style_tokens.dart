import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';
import 'package:woo_test/design/app_colors.dart';

@Singleton()
class DesignTextStyleTokens {
  TextStyle tickerText() {
    return const TextStyle(
      color: AppColors.textMainColor,
      fontSize: 14.0,
    );
  }

  TextStyle tickerInfo() {
    return const TextStyle(
      color: AppColors.textSecondaryColor,
      fontSize: 14.0,
    );
  }

  TextStyle tickerIconText() {
    return const TextStyle(
      color: AppColors.textMainColor,
      fontSize: 12.0,
    );
  }
}
