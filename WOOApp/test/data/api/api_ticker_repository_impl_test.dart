import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart' as di;
import 'package:woo_test/data/api/responses/api_ticker_item.dart';
import 'package:woo_test/domain/repository/api/api_ticker_repository.dart';
import 'package:woo_test/presentation/injection/configure_dependencies.dart';

void main() {
  Future<void> _prepareGeneralMocks() async {
    await GetIt.instance.reset();
    await configureDependencies(di.Environment.test);
    await GetIt.instance.allReady();
  }

  group('tickets', () {
    test('get success', () async {
      // prepare mock
      await _prepareGeneralMocks();

      // test
      final ApiTickerRepository underTest = getIt<ApiTickerRepository>();
      final List<ApiTickerItem> result = await underTest.tickers();

      // check
      expect(result, isNot(null));
      expect(result.isNotEmpty, equals(true));

      for (final item in result) {
        expect(item.base.isNotEmpty, equals(true));
        expect(item.quote.isNotEmpty, equals(true));
        expect(item.quote.isNotEmpty, equals(true));
        expect(item.lastPrice, greaterThanOrEqualTo(0));
        expect(item.volume, greaterThanOrEqualTo(0));
      }
    });
  });
}
