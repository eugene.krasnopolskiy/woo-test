# woo_test

WOO Test Application

## Preparing Android release builds

1. To prepare the release APK execute the command:

   flutter build apk --release --obfuscate --split-debug-info --split-per-abi

The APK file should be located by path: LocalsApp/build/app/outputs/apk/release/app-release.apk

2. To prepare the release AAB execute the command:

   flutter build appbundle --release --obfuscate --split-debug-info --split-per-abi

The AAB file should be located by path: LocalsApp/build/app/outputs/bundle/release/app-release.aab

## Preparing iOS release builds

1. To prepare the release build execute the command:

   flutter build ios --release

2. Open LocalsApp/ios/Runner.xcworkspace in Xcode. Go to Product -> Archive. When archiving will be completed select Distribute.


# Helper commands

## For generate classes
flutter packages pub run build_runner build --delete-conflicting-outputs

## Code analyze and testing
dart analyze
flutter test